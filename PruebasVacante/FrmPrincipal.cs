﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace PruebasVacante
{
    public partial class FrmPrincipal : Form
    {
        OMR omr = new OMR();
        WsC ws = new WsC();

        DataTable dt = new DataTable();

        bool DESC_ASC = false;

        public FrmPrincipal()
        {
            InitializeComponent();
            //loadComboBox();
        }

        #region BOTONES
        #region MENU LATERAL
        private void BtnOrdenarLista_Click(object sender, EventArgs e)
        {
            ActionBtn.fontStyleNormal(this);
            ActionBtn.backColor(this);
            btnOrdenarTabla.BackColor = Color.FromArgb(45, 45, 48);
            btnOrdenarTabla.Font = new Font(btnOrdenarTabla.Font, FontStyle.Bold);

            DataView dv = new DataView(dt);//(dt, "codigo like '%'", "codigo ASC", DataViewRowState.OriginalRows);
            if (DESC_ASC)
            {
                dv.Sort = "codigo ASC";
                DESC_ASC = false;
            }
            else
            {
                dv.Sort = "codigo DESC";
                DESC_ASC = true;
            }

            dgvJSON.DataSource = dv;
        }

        private void BtnOMR_Click(object sender, EventArgs e)
        {
            ActionBtn.fontStyleNormal(this);
            ActionBtn.backColor(this);
            btnOMR.BackColor = Color.FromArgb(45, 45, 48);
            btnOMR.Font = new Font(btnOMR.Font, FontStyle.Bold);

            pbOMR.Image = omr.Load("");

            pbOMR.Visible = true;
            dgvJSON.Visible = false;
        }

        private void BtnProcesandoJSON_Click(object sender, EventArgs e)
        {
            ActionBtn.fontStyleNormal(this);
            ActionBtn.backColor(this);
            btnProcesandoJSON.BackColor = Color.FromArgb(45, 45, 48);
            btnProcesandoJSON.Font = new Font(btnProcesandoJSON.Font, FontStyle.Bold);

            ws.json = File.ReadAllText("data.json");

            dgvJSON.Visible = true;
            pbOMR.Visible = false;

            dt = ws.dataTable();

            dgvJSON.DataSource = dt;//ws.dataTable();
        }

        private void BtnWS_Click(object sender, EventArgs e)
        {
            string url = "https://localhost:44396/api/Docente/1";

            WebClient wc = new WebClient();

            ws.json = wc.DownloadString(url);

            dt = ws.dataTable();

            dgvJSON.DataSource = dt;
        }

        #endregion

        #endregion

        #region METODOS
        /*public void loadComboBox()
        {
            DirectoryInfo di = new DirectoryInfo("imagenes_omr");
            
            foreach (var files in di.GetFiles())
            {
                cmbDiseñosOMR.Items.Add(files.Name);//Console.WriteLine(fi.Name);
            }
        }*/
        #endregion
    }
}
