﻿namespace PruebasVacante
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pPrincipal = new System.Windows.Forms.Panel();
            this.btnWS = new System.Windows.Forms.Button();
            this.btnOMR = new System.Windows.Forms.Button();
            this.lDepartamento = new System.Windows.Forms.Label();
            this.btnProcesandoJSON = new System.Windows.Forms.Button();
            this.btnOrdenarTabla = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgvJSON = new System.Windows.Forms.DataGridView();
            this.pbOMR = new System.Windows.Forms.PictureBox();
            this.cmbDiseñosOMR = new System.Windows.Forms.ComboBox();
            this.pPrincipal.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJSON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOMR)).BeginInit();
            this.SuspendLayout();
            // 
            // pPrincipal
            // 
            this.pPrincipal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(70)))), ((int)(((byte)(139)))));
            this.pPrincipal.Controls.Add(this.btnWS);
            this.pPrincipal.Controls.Add(this.btnOMR);
            this.pPrincipal.Controls.Add(this.lDepartamento);
            this.pPrincipal.Controls.Add(this.btnProcesandoJSON);
            this.pPrincipal.Controls.Add(this.btnOrdenarTabla);
            this.pPrincipal.Dock = System.Windows.Forms.DockStyle.Left;
            this.pPrincipal.Location = new System.Drawing.Point(0, 0);
            this.pPrincipal.Name = "pPrincipal";
            this.pPrincipal.Size = new System.Drawing.Size(166, 450);
            this.pPrincipal.TabIndex = 11;
            // 
            // btnWS
            // 
            this.btnWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(70)))), ((int)(((byte)(139)))));
            this.btnWS.FlatAppearance.BorderSize = 0;
            this.btnWS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btnWS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWS.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWS.ForeColor = System.Drawing.Color.White;
            this.btnWS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnWS.Location = new System.Drawing.Point(0, 277);
            this.btnWS.Name = "btnWS";
            this.btnWS.Size = new System.Drawing.Size(166, 55);
            this.btnWS.TabIndex = 12;
            this.btnWS.Text = "Web Services";
            this.btnWS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnWS.UseVisualStyleBackColor = false;
            this.btnWS.Click += new System.EventHandler(this.BtnWS_Click);
            // 
            // btnOMR
            // 
            this.btnOMR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(70)))), ((int)(((byte)(139)))));
            this.btnOMR.FlatAppearance.BorderSize = 0;
            this.btnOMR.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btnOMR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOMR.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOMR.ForeColor = System.Drawing.Color.White;
            this.btnOMR.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOMR.Location = new System.Drawing.Point(0, 222);
            this.btnOMR.Name = "btnOMR";
            this.btnOMR.Size = new System.Drawing.Size(166, 55);
            this.btnOMR.TabIndex = 11;
            this.btnOMR.Text = "OMR";
            this.btnOMR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOMR.UseVisualStyleBackColor = false;
            this.btnOMR.Click += new System.EventHandler(this.BtnOMR_Click);
            // 
            // lDepartamento
            // 
            this.lDepartamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lDepartamento.AutoSize = true;
            this.lDepartamento.Font = new System.Drawing.Font("Segoe UI Symbol", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lDepartamento.ForeColor = System.Drawing.Color.White;
            this.lDepartamento.Location = new System.Drawing.Point(92, 434);
            this.lDepartamento.Name = "lDepartamento";
            this.lDepartamento.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lDepartamento.Size = new System.Drawing.Size(64, 13);
            this.lDepartamento.TabIndex = 4;
            this.lDepartamento.Text = "Versión 1.0";
            this.lDepartamento.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnProcesandoJSON
            // 
            this.btnProcesandoJSON.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(70)))), ((int)(((byte)(139)))));
            this.btnProcesandoJSON.FlatAppearance.BorderSize = 0;
            this.btnProcesandoJSON.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btnProcesandoJSON.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcesandoJSON.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcesandoJSON.ForeColor = System.Drawing.Color.White;
            this.btnProcesandoJSON.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProcesandoJSON.Location = new System.Drawing.Point(0, 167);
            this.btnProcesandoJSON.Name = "btnProcesandoJSON";
            this.btnProcesandoJSON.Size = new System.Drawing.Size(166, 55);
            this.btnProcesandoJSON.TabIndex = 10;
            this.btnProcesandoJSON.Text = "Procesando JSON";
            this.btnProcesandoJSON.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnProcesandoJSON.UseVisualStyleBackColor = false;
            this.btnProcesandoJSON.Click += new System.EventHandler(this.BtnProcesandoJSON_Click);
            // 
            // btnOrdenarTabla
            // 
            this.btnOrdenarTabla.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(70)))), ((int)(((byte)(139)))));
            this.btnOrdenarTabla.FlatAppearance.BorderSize = 0;
            this.btnOrdenarTabla.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btnOrdenarTabla.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrdenarTabla.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrdenarTabla.ForeColor = System.Drawing.Color.White;
            this.btnOrdenarTabla.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOrdenarTabla.Location = new System.Drawing.Point(0, 112);
            this.btnOrdenarTabla.Name = "btnOrdenarTabla";
            this.btnOrdenarTabla.Size = new System.Drawing.Size(166, 55);
            this.btnOrdenarTabla.TabIndex = 9;
            this.btnOrdenarTabla.Text = "Ordenar Tabla";
            this.btnOrdenarTabla.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOrdenarTabla.UseVisualStyleBackColor = false;
            this.btnOrdenarTabla.Click += new System.EventHandler(this.BtnOrdenarLista_Click);
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(166, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(634, 78);
            this.panel2.TabIndex = 13;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvJSON);
            this.panel3.Controls.Add(this.pbOMR);
            this.panel3.Controls.Add(this.cmbDiseñosOMR);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(166, 78);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(634, 372);
            this.panel3.TabIndex = 13;
            // 
            // dgvJSON
            // 
            this.dgvJSON.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvJSON.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvJSON.Location = new System.Drawing.Point(0, 33);
            this.dgvJSON.Name = "dgvJSON";
            this.dgvJSON.Size = new System.Drawing.Size(634, 339);
            this.dgvJSON.TabIndex = 3;
            // 
            // pbOMR
            // 
            this.pbOMR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbOMR.Location = new System.Drawing.Point(0, 33);
            this.pbOMR.Name = "pbOMR";
            this.pbOMR.Size = new System.Drawing.Size(634, 339);
            this.pbOMR.TabIndex = 1;
            this.pbOMR.TabStop = false;
            // 
            // cmbDiseñosOMR
            // 
            this.cmbDiseñosOMR.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbDiseñosOMR.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDiseñosOMR.FormattingEnabled = true;
            this.cmbDiseñosOMR.Location = new System.Drawing.Point(0, 0);
            this.cmbDiseñosOMR.Name = "cmbDiseñosOMR";
            this.cmbDiseñosOMR.Size = new System.Drawing.Size(634, 33);
            this.cmbDiseñosOMR.TabIndex = 2;
            this.cmbDiseñosOMR.Visible = false;
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pPrincipal);
            this.Name = "FrmPrincipal";
            this.Text = "Principal";
            this.pPrincipal.ResumeLayout(false);
            this.pPrincipal.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvJSON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOMR)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pPrincipal;
        private System.Windows.Forms.Button btnWS;
        private System.Windows.Forms.Button btnOMR;
        private System.Windows.Forms.Label lDepartamento;
        private System.Windows.Forms.Button btnProcesandoJSON;
        private System.Windows.Forms.Button btnOrdenarTabla;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pbOMR;
        private System.Windows.Forms.ComboBox cmbDiseñosOMR;
        private System.Windows.Forms.DataGridView dgvJSON;
    }
}

