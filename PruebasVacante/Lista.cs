﻿public enum NivelAcceso
{
    ADMINISTRADOR,
    ADMINISTRADORMM,
    SUPERVISOR,
    CAJERO,
    COBRANZA,
    VENDEDOR,
    SISTEMAS 
}

public enum action
{
    insert,
    query,
    update,
    delete,
    free
}

public enum messageResponse
{
    conectionFail,
    actionFail,
    actionSuccess,
    fieldsRequired,
    allActionFail,
}

public enum menuPrincipal
{
    btnOrdenVenta,
    btnAlmacen,
    btnProcesos,
    btnUsuarios,
    btnCliente,
    btnProducto
}

public enum estatus
{
    Activa,
    Fabricacion,
    Empacada,
    Enviada,
    Finalizada
}