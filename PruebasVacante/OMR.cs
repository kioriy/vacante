﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Reflection;

using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;
//using Point = System.Drawing.Point;

namespace PruebasVacante
{
    public class OMR
    {
        /// <summary>
        /// Desencadena el conteo de las burbujas en una imagen, recibe como parametro la ruta de la imagen.
        /// </summary>
        /// <param name="pathFile"></param>
        public Bitmap Load(string pathFile)
        {
            //Assembly assembly = this.GetType().Assembly;
            Bitmap image = new Bitmap($@"imagenes_omr\test.jpg");
            return ProcessImage(image);
        }

        /// <summary>
        /// Proceso para crear un filtro en la imagen, contar las burbujas y crear un contorno recibe como parametro
        /// el mapa de bits de la imagen.
        /// </summary>
        /// <param name="bitmap"></param>
        private Bitmap ProcessImage(Bitmap bitmap)
        {
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),ImageLockMode.ReadWrite, bitmap.PixelFormat);

            ColorFiltering colorFilter = new ColorFiltering();

            colorFilter.Red = new IntRange(0, 80);
            colorFilter.Green = new IntRange(0, 80);
            colorFilter.Blue = new IntRange(0, 80);
            colorFilter.FillOutsideRange = false;

            colorFilter.ApplyInPlace(bitmapData);

            BlobCounter blobCounter = new BlobCounter();

            blobCounter.FilterBlobs = true;
            blobCounter.MinHeight = 5;
            blobCounter.MinWidth = 5;

            blobCounter.ProcessImage(bitmapData);
            Blob[] blobs = blobCounter.GetObjectsInformation();
            bitmap.UnlockBits(bitmapData);

            SimpleShapeChecker shapeChecker = new SimpleShapeChecker();

            Graphics g = Graphics.FromImage(bitmap);
            Pen yellowPen = new Pen(Color.Yellow, 2);

            for (int i = 0, n = blobs.Length; i < n; i++)
            {
                List<IntPoint> edgePoints = blobCounter.GetBlobsEdgePoints(blobs[i]);

                AForge.Point center;
                float radius;

                //Si la figura es un circulo se dibuja el cotorno para resaltar
                if (shapeChecker.IsCircle(edgePoints, out center, out radius))
                {
                    g.DrawEllipse(yellowPen,
                        (float)(center.X - radius), (float)(center.Y - radius),
                        (float)(radius * 2), (float)(radius * 2));
                }
            }

            yellowPen.Dispose();
      
            Clipboard.SetDataObject(bitmap);
           
            return bitmap;
        }
    }
}
