﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Data;

namespace PruebasVacante
{
    public class WsC
    {
        //ws.PVT ws = new ws.PVT();
 
        public string json = "";
        public DataTable dt;

        /*public async Task<bool> executeAsync(string table, string values, string action, string where)
        {
            try
            {
                ws.Timeout = 900000;
                json = ws.query(table, values, action, where);

                if (json != "[null]" & json != "" & json != "false")
                {
                    Mensaje.getMessage(messageResponse.actionSuccess.ToString());
                    return true;
                }
                else
                {
                    Mensaje.getMessage(messageResponse.actionFail.ToString());
                    return false;
                }
            }
            catch (System.Net.WebException)
            {
                Mensaje.getMessage(messageResponse.conectionFail.ToString());
                return false;
            }
            catch (System.InvalidOperationException)
            {
                Mensaje.getMessage(messageResponse.allActionFail.ToString());
                return false;
            }
        }*/

        public List<T> list<T>()
        {
            if (json != "false")
            {
                return JsonConvert.DeserializeObject<List<T>>(json);
            }
            else
            {
                return null;
            }
        }

        public string serializeDictionary(Dictionary<string, string> formData)
        {
            if (json != "false")
            {
                return JsonConvert.SerializeObject(formData);
            }
            else
            {
                return null;
            }
        }

        public T deserializeObjet<T>(string json)
        {
            if (json != "false")
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            else
            {
                return default(T);
            }
        }

        public DataTable dataTable()
        {
            if (json != "false")
            {
                dt = new DataTable();
                dt = (DataTable)JsonConvert.DeserializeObject(json, typeof(DataTable));

                return dt;
            }
            else
            {
                return null;
            }
        }
    }
}
    