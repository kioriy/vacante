﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace PruebasVacante
{
    public static class ActionForm
    {
        public static Dictionary<string, string> values;

        public static void clear(Form form)
        {
            foreach (Control control in form.Controls)
            {
                if (control is TextBox | control is ComboBox)
                {
                    control.Text = "";
                }
            }
        }

        public static bool isEmpty(Form form)
        {
            foreach (Control control in form.Controls)
            {
                if ((control is TextBox | control is ComboBox) & control.ForeColor == Color.Gray)// control.Text == String.Empty & control.ForeColor == Color.Gray)
                {
                    if (evaluarVacio(control))
                    {
                        Mensaje.getMessage(messageResponse.fieldsRequired.ToString());
                        return true;
                    }
                    else
                    {
                        return false;
                    }                   
                }
            }
            return false;
        }

        #region PLACEHOLDER METHODS
        public static void placeHolder(Form form)
        {
            foreach (Control control in form.Controls)
            {
                if (control is TextBox | control is ComboBox)// & control.Text == String.Empty | control is ComboBox & control.Text == String.Empty)
                {
                    control.GotFocus += new EventHandler(RemoveText);
                    control.LostFocus += new EventHandler(AddText);

                    control.Text = placeHolderName(control);
                    control.Text.ToLower();
                }
            }
        }

        public static void RemoveText(object sender, EventArgs e)
        {
            Control textBox = sender as Control;
            
            if (textBox.ForeColor == Color.Gray)
            {
                textBox.Text = "";
                textBox.ForeColor = Color.Black;
            }
        }

        public static void AddText(object sender, EventArgs e)
        {
            Control textBox = sender as Control;
            
            if (string.IsNullOrWhiteSpace(textBox.Text))
            {
                textBox.Text = placeHolderName(textBox);//textBox.Name.Substring(3);//textBoxName();
            }
        }

        public static string placeHolderName(Control textBox)
        {
            textBox.ForeColor = Color.Gray;
            textBox.Font = new Font(new FontFamily("Segoe UI Symbol"), textBox.Font.Size);

            return remplaceCharater(textBox);
            /*if (evaluarVacio(textBox))
            {
                return textBox.Name.Substring(3).Replace('_', ' ');
            }
            else
            {
                string nombre = textBox.Name.Substring(3, textBox.Name.Length - 9).Replace('_', ' ');
                return nombre;//textBox.Name.Substring(3, textBox.Name.Length - 5).Replace('_',' ');
            }*/
        }

        public static string remplaceCharater(Control control)
        {
            if (evaluarVacio(control))
            {
                return control.Name.Substring(3).Replace('_', ' ');
            }
            else
            {
                return control.Name.Substring(3, control.Name.Length - 9).Replace('_', ' ');//nombre = control.Name.Substring(3, control.Name.Length - 9).Replace('_', ' ');
                //return nombre;//textBox.Name.Substring(3, textBox.Name.Length - 5).Replace('_',' ');
            }
        }
        #endregion

        static public bool evaluarVacio(Control textBox)
        {
            string[] cadena = textBox.Name.Split('_');

            if(cadena.Last() == "false")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static Dictionary<string, string> loadValues(Form form)
        {
            values = new Dictionary<string, string>();

            foreach (Control control in form.Controls)
            {
                if (control is TextBox | control is ComboBox)
                {
                    values.Add(remplaceCharater(control).ToLower().Replace(' ', '_'), control.Text);
                }
            }
            return values;
        }
    }
}
