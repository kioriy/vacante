﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VacanteTest.Models
{
    public class Docente
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string cct { get; set; }
        public string materia { get; set; }
    }
}