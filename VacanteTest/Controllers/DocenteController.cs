﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VacanteTest.Models;

namespace VacanteTest.Controllers
{
    public class DocenteController : ApiController
    {
        Docente[] docentes = new Docente[] 
        {
            new Docente{ id = 1, nombre = "Jorge Benavides", cct = "123456", materia = "Informatica"},
            new Docente{ id = 2, nombre = "Arturo Mejia", cct = "783874", materia = "Deportes"},
            new Docente{ id = 3, nombre = "Vero Gomez", cct = "647839", materia = "Costura"},
            new Docente{ id = 4, nombre = "Alan Mercenario", cct = "974973", materia = "Matematicas"},
        };

        public IEnumerable<Docente> All()
        {
            return docentes;
        }

        public IHttpActionResult GetDocente(int id)
        {
            var docente = docentes.FirstOrDefault(x => x.id == id);

            if(docente != null)
            {
                return Ok(docente);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
